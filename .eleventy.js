module.exports = config =>
{
	/*
	 * Collections
	 */

	/*
	 * Transforms
	 */

	/*
	 * Filters
	 */
	const filtersDir = './src/site/_filters';
	config.addFilter( "babel", require( `${filtersDir}/babel` ) );
	config.addFilter( "cssmin", require( `${filtersDir}/cssmin` ) );

	/*
	 * Shortcodes
	 */
	const shortcodesDir = './src/site/_shortcodes';
	config.addPairedShortcode( "smartquotes", require( `${shortcodesDir}/smartquotes` ) );
	config.addShortcode( "emoji", require( `${shortcodesDir}/emoji` ) );

	/*
	 * Miscellaneous
	 */
	config.addPassthroughCopy( "src/site/images" );

	return {
		dir: {
			input: "src/site",
			output: "dist",
		},

		templateFormats: ["css", "md", "njk"],
	};
};
