const babel = require("@babel/core");

module.exports = string =>
{
	let options = require( "../../../babel.config.json" );
	return babel.transform( string, options ).code;
};
