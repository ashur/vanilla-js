module.exports = {
	title: "Vanilla JavaScript",
	emoji: {
		character: "🍦",
		label: "ice cream"
	},
	subtitle: "Projects for the Vanilla JS Academy",
};
