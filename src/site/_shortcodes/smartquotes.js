const smartquotes = require( "smartquotes" );

module.exports = string => smartquotes.string( string );
