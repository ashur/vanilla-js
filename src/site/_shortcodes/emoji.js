module.exports = (emoji, label) =>
{
	return `<span role="img" aria-label="${label}">${emoji}</span>`;
};
